class Transformer {
    
    constructor(name, health = 100){
        this.name = name;
        this.health = health;
    }

    attack(){

    }

    hit(weapon){
        this.health -= weapon.damage;
    }

}

class Autobot extends Transformer {

    constructor (name, weapon){
        super(name);
        this.weapon = weapon;
    }

    attack(){
        return this.weapon.fight();
    }

}

class Decepticon extends Transformer {

    // constructor(name, health){
    // }
    attack(){
        return {damage:5, speed:1000}
    }

}

class Weapon{
    constructor(damage, speed){
        this.damage = damage;
        this.speed = speed;
    }

    fight(){
        return{damage: this.damage, speed: this.speed}
    }
}

// const transformer1 = new Autobot('Optimus Prime', new Weapon(100, 1000));
// console.log(transformer1);

// const transformer2 = new Decepticon('Megatron', 10000);
// console.log(transformer2);

// transformer1.hit(transformer2.attack());
// console.log(transformer1);

// transformer2.hit(transformer1.attack());
// console.log(transformer2);

class Arena{

    constructor(side1, side2, visualizer){
        this.side1 = side1; 
        this.side2 = side2; //будем считать, что бот со стороны 2 один.
        this.visualizer = visualizer;
    }

    // Шаг 1:
    //     Сторона 1 атакует Сторону 2. 
    // Шаг 2:
    //     Проверка условия победы стороны 1. 
    //     Проверить, есть ли живые боты в стороне 2. 
    // Шаг 3:
    //     Атаковать Сторона 2 атакует Сторону 1. 
    // Шаг 4:
    //     Проверить условие победы стороны 2.
    start() {
        
        const timer = setInterval( () => {

                //Шаг 1
                // this.side1.forEach(bot=>this.side2[0].hit(bot.attack()));
                this.side1.forEach(bot=>this.getBotWithMaxHealth(this.side2).hit(bot.attack()));
                
                //Шаг 2
                this.side2 = this.side2.filter(bot => bot.health > 0);
                if (!this.side2.length){
                    console.log('Сторона 1 победила!');
                    clearInterval(timer);
                }
                
                //3
                // this.side2.forEach(bot=>this.getBotWithMaxHealth(side1).hit(bot.attack()));
                this.side2.forEach(bot=>this.getBotWithMaxHealth(this.side1).hit(bot.attack()));
                
                //4
                this.side1 = this.side1.filter(bot => bot.health > 0);
                if (!this.side1.length){
                    console.log('Сторона 2 победила!');
                    clearInterval(timer);
                }

                this.visualizer.render(this.side1.map(bot => bot.health), 
                                  this.side2.map(bot => bot.health));

        }, 1000);

    }
    
    //создадим еще один метод, использующий параметр speed
    startWithSpeed(){
        const renderInterval = setInterval(()=>{

            this.visualizer.render(this.side1.map(bot => bot.health), this.side2.map(bot => bot.health));
            
            if (!this.side1.length || !this.side2.length){
                clearInterval(renderInterval);
            }

        },0);

        this.fight(this.side1, this.side2);

        this.fight(this.side2, this.side1);
        
    }

    //side1 - атакующая сторона
    fight(side1, side2){
        side1.forEach(bot=>{

            const interval = setInterval( () => {
                
                if (!bot || !bot.health || bot.health < 0){
                    clearInterval(interval);
                    return;
                }
                
                if (!side2.length) {
                    clearInterval(interval);
                    return;
                }

                const botFromSide2 = this.getBotWithMaxHealth(side2);
                if (botFromSide2) {
                    botFromSide2.hit(bot.attack());

                    if (side2 === this.side1){
                        side2 = this.side1 = side2.filter(bot => bot.health > 0);
                    }
                    if (side2 === this.side2){
                        side2 = this.side2 = side2.filter(bot => bot.health > 0);
                    }

                }

            }, bot.attack().speed)
            
        });
    }


    getBotWithMaxHealth(array){
        let botWithMaxHealth = array[0];
        array.forEach(element => {
            if (botWithMaxHealth.health < element.health) {
                botWithMaxHealth = element;    
            }
        });
        return botWithMaxHealth;
    }


}

class Visualizer{

    constructor(){}

    render(healthSide1, healthSide2) {

        const Side1El = document.querySelector('.js-arena-side-1');
        const Side2El = document.querySelector('.js-arena-side-2');
        
        Side1El.innerHTML = 
            healthSide1.map(
                health => `<div class="bot"><span>${health} hp</span></div>`
                );

        Side2El.innerHTML = 
                healthSide2.map(
                    health => `<div class="bot"><span>${health} hp</span></div>`
                    );                
    
    }

}

const area = new Arena(
    
    [ new Autobot('Bot1', new Weapon(100,1000)), 
      new Autobot('Bot2', new Weapon(100,1000)), 
      new Autobot('Bot3', new Weapon(100,1000)),
      new Autobot('Bot4', new Weapon(100,1000))
    ], 

    [ new Decepticon('Metatron', 10000), 
      new Decepticon('Metatron2', 10000) 
    ],

    new Visualizer

)

//area.start();
area.startWithSpeed();